import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Applogo} from '../Entryfile/imagepath.jsx'
import { auth } from '../Firebase/index.js';

import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

let RegistrationPage = () => {
  let [email, setEmail] = React.useState('');
  let [password, setPassword] = React.useState('');
  let [password2, setPassword2] = React.useState('');
  let [message, setMessage] = React.useState('');
  let [signUpBtn, setsignUpBtn] = React.useState('Register');
  let [open, setOpen] = React.useState(false);
  let handleClick = () => {
    setOpen(true);
  };
  let handleClose = (event, reason) => {
    if (reason === 'clickaway') {
        return;
    }
    setOpen(false);
  }

  let signUp = () => {
    if (password !== password2) {
      handleClick();
      setMessage("Password Not Matched");
      setPassword('');
      setPassword2('');
    } else {
      auth.createUserWithEmailAndPassword(email, password)
        .then(() => {
          var user = auth.currentUser;
          user.sendEmailVerification().then(function() {
            window.location.href = "/login?v=e";
          }).catch(function(error) {
            console.error(error);
          });
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(errorCode, errorMessage);
        });
    }
  }

  

  let onApplyJob = () => {
    e.preventDefault();
  }

  return (
    <>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message={message}
        action={
          <React.Fragment>
            <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
      <div className="main-wrapper">
        <Helmet>
            <title>Register - HRMS Admin Template</title>
            <meta name="description" content="Login page"/>					
        </Helmet> 
        <div className="account-content">
          <a href="/light/applyjob/joblist" className="btn btn-primary apply-btn">Apply Job</a>
          <div className="container">
            <div className="account-logo">
              <a href="/"><img className="w-25" src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1610042386/onlinesaas/Logo_upw7zu.png" alt="Dreamguy's Technologies" /></a>
            </div>
            <div className="account-box">
              <div className="account-wrapper">
                <h3 className="account-title">Register</h3>
                <p className="account-subtitle">Access to our dashboard</p>
                <form action="/light/app/main/dashboard">
                  <div className="form-group">
                    <label>Email</label>
                    <input className="form-control" value={email} onChange={(event) => setEmail(event.target.value)} type="text" />
                  </div>
                  <div className="form-group">
                    <label>Password</label>
                    <input className="form-control" value={password} onChange={(event) => setPassword(event.target.value)} type="password" />
                  </div>
                  <div className="form-group">
                    <label>Repeat Password</label>
                    <input className="form-control" value={password2} onChange={(event) => setPassword2(event.target.value)} type="password" />
                  </div>
                  <div className="form-group text-center">
                    <button onClick={signUp} className="btn btn-primary account-btn" type="button">{signUpBtn}</button>
                  </div>
                  <div className="account-footer">
                    <p>Already have an account? <a href="/light/login">Login</a></p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default RegistrationPage
