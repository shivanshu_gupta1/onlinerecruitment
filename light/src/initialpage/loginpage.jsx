import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Applogo} from '../Entryfile/imagepath.jsx'
import { auth } from '../Firebase/index.js';
import queryString from 'query-string';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

let loginpage = ({ location }) => {
  let [email, setEmail] = React.useState('');
  let [password, setPassword] = React.useState('');
  let [verifyEmail, setverifyEmail] = React.useState(false);
  let [message, setMessage] = React.useState('');
  let [open, setOpen] = React.useState(false);

  let handleClick = () => {
    setOpen(true);
  };
  let handleClose = (event, reason) => {
    if (reason === 'clickaway') {
        return;
    }
    setOpen(false);
  }

  React.useEffect(() => {
    let { v } = queryString.parse(location.search);
    if (v === "e") {
      setverifyEmail(true);
    } else {
      setverifyEmail(false);
    }
  }, []);

  let login = () => {
    auth.signInWithEmailAndPassword(email, password)
      .then(() => {
          auth.onAuthStateChanged(function(user) {
              if (user) {
                sessionStorage.setItem("userId", user.uid);
                sessionStorage.setItem("email", user.email);
                window.location.href = "/light/app/main/dashboard";
              } else {
                  console.log("No User");
              }
          });
      })
      .catch(function(error) {
          var errorCode = error.code;
          if ( errorCode === "auth/wrong-password" ) {
              handleClick();
              setMessage('Wrong Password');
              setPassword('');
              setEmail('');
          } else if ( errorCode === "auth/user-not-found" ) {
              handleClick();
              setMessage('No User Found');
              setPassword('');
              setEmail('');
          }
      });
  }

  return (
    <>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message={message}
        action={
          <React.Fragment>
            <Button color="secondary" size="small" onClick={handleClose}>
              UNDO
            </Button>
            <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
      <div className="main-wrapper">
        <Helmet>
          <title>Login - HRMS Admin Template</title>
           <meta name="description" content="Login page"/>					
        </Helmet>
        <div className="account-content">
          <a href="/light/applyjob/joblist" className="btn btn-primary apply-btn">Apply Job</a>
          <div className="container">
            {/* Account Logo */}
            <div className="account-logo">
              <a href="/"><img className="w-25" src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1610042386/onlinesaas/Logo_upw7zu.png" alt="Dreamguy's Technologies" /></a>
            </div>
            {/* /Account Logo */}
            <div className="account-box">
              <div className="account-wrapper">
                {
                  verifyEmail ? (
                    <Alert severity="info">Please Verify Your Email</Alert>
                  ) :  null
                }
                <h3 className="account-title">Login</h3>
                <p className="account-subtitle">Access to our dashboard</p>
                {/* Account Form */}
                <form action="/light/app/main/dashboard">
                  <div className="form-group">
                    <label>Email Address</label>
                    <input className="form-control" onChange={(event) => setEmail(event.target.value)} type="text" />
                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col">
                        <label>Password</label>
                      </div>
                      <div className="col-auto">
                        <a className="text-muted" href="/light/forgotpassword">
                          Forgot password?
                        </a>
                      </div>
                    </div>
                    <input className="form-control" onChange={(event) => setPassword(event.target.value)} type="password" />
                  </div>
                  <div className="form-group text-center">
                    <a className="btn btn-primary account-btn" onClick={login}>
                    Login</a>
                  </div>
                  <div className="account-footer">
                    <p>Don't have an account yet? <a href="/light/register">Register</a></p>
                  </div>
                </form>
                {/* /Account Form */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default loginpage
